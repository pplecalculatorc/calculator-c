﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

//test
namespace WindowsFormsApplication1
{
    public partial class CalcForm : Form
    {
        public CalcForm()
        {
            InitializeComponent();

            FlowLayoutPanel panel = new FlowLayoutPanel();
            panel.AutoSize = true;
            panel.FlowDirection = FlowDirection.TopDown;
            panel.Controls.Add(DisplayTextBox);
            this.Controls.Add(panel);


            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(CalcForm_KeyPress);
            this.coba = 0;
            DisplayTextBox.KeyPress += new KeyPressEventHandler(DisplayTextBox_KeyPress);
            DisplayTextBox.MaxLength = 12;
            //DisplayTextBox.KeyPress += new KeyPressEventHandler(DisplayTextBox_KeyPress);
        }


        private decimal firstNum = 0.0m;
        private decimal secondNum = 0.0m;
        private decimal result = 0.0m;
        private decimal save = 0.0m;
        private int coba;
        private int operatorType = (int)MathOperations.NoOperator;

        //public decimal Temp;
        
        public void reset()
        {
            DisplayTextBox.Text = "0";
            firstNum = 0.0m;
            secondNum = 0.0m;
            result = 0;
            operatorType = (int)MathOperations.NoOperator;
        }

        public enum MathOperations
        {
            NoOperator = 0,
            Add = 1,
            Minus = 2,
            Divide = 3,
            Multiply = 4
        }

        //operators functions
        private void DigitBtn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //Button Temp = (Button)sender;

            if (DisplayTextBox.Text.Length == DisplayTextBox.MaxLength)
            {
                MessageBox.Show("Digit Limit Exceeded");
            }
            else
            {
                if (DisplayTextBox.Text == "0")
                {
                    DisplayTextBox.Clear();
                }
                //(string)Temp = btn.Text;            
                DisplayTextBox.Text += btn.Text;

            }
        }

        private void PlusButton_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperations.Add);
        }

        private void MinusButton_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperations.Minus);
        }

        private void DivideButton_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperations.Divide);
        }

        private void MultiplyButton_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperations.Multiply);
        }

        private void SaveValueAndOperatorType(int operation)
        {
            operatorType = operation;
            firstNum = Convert.ToDecimal(DisplayTextBox.Text);
            DisplayTextBox.Text = "0";
        }

        private void PlusMinusButton_Click(object sender, EventArgs e)
        {
            if (!DisplayTextBox.Text.Contains('-') && (DisplayTextBox.Text == "0") && (DisplayTextBox.Text!=null)) //(DisplayTextBox.Text.Contains('0')) -> Jika data = 0, replace dengan -
            {                                                                                                             //(DisplayTextBox.Text!=null)         -> Jika berisi data, ignore '-'              
                DisplayTextBox.Text = DisplayTextBox.Text.Trim('0');
                DisplayTextBox.Text = "-" + DisplayTextBox.Text;
            }

            //else if (!DisplayTextBox.Text.Contains('-') && (DisplayTextBox.Text== "0"))
            //{
              //  DisplayTextBox.Clear();
               // DisplayTextBox.Text = "-" + DisplayTextBox.Text;
            //}

            else
            {
                DisplayTextBox.Text = DisplayTextBox.Text.Trim('-');
            }
            
        }

        private void DotButton_Click(object sender, EventArgs e)
        {
            if (!DisplayTextBox.Text.Contains('.'))
            {
                DisplayTextBox.Text += ".";
            }
            
        }

        private void EqualButton_Click(object sender, EventArgs e)
        {
            secondNum = Convert.ToDecimal(DisplayTextBox.Text);

                switch (operatorType)
            {
                case (int)MathOperations.Add:
                    result = firstNum + secondNum;
                    break;

                case (int)MathOperations.Minus:
                    result = firstNum - secondNum;
                    break;

                case (int)MathOperations.Divide:
                    if (secondNum == 0)
                    {
                        MessageBox.Show("Tidak bisa membagi dengan 0.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        result = firstNum / secondNum;
                    }
                    break;

                case (int)MathOperations.Multiply:
                    result = firstNum * secondNum;
                    break;
            }
            DisplayTextBox.Text = result.ToString();
        }

        private void buttonhapus_Click(object sender, EventArgs e)
        {
            //DisplayTextBox.Text = DisplayTextBox.Text.Trim((char)Temp);
            DisplayTextBox.Text = "0";
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void CalcForm_KeyPress(object sender, KeyPressEventArgs e)
        {
                switch (e.KeyChar)
                {
                    case (char)'0':
                        ZeroButton.PerformClick();
                        break;
                    case (char)'1':
                        OneButton.PerformClick();
                        break;
                    case (char)'2':
                        TwoButton.PerformClick();
                        break;
                    case (char)'3':
                        ThreeButton.PerformClick();
                        break;
                    case (char)'4':
                        FourButton.PerformClick();
                        break;
                    case (char)'5':
                        FiveButton.PerformClick();
                        break;
                    case (char)'6':
                        SixButton.PerformClick();
                        break;
                    case (char)'7':
                        SevenButton.PerformClick();
                        break;
                    case (char)'8':
                        EightButton.PerformClick();
                        break;
                    case (char)'9':
                        NineButton.PerformClick();
                        break;
                    case (char)'.':
                        DotButton.PerformClick();
                        break;
                    case (char)'+':
                        PlusButton.PerformClick();
                        break;
                    case (char)'-':
                        MinusButton.PerformClick();
                        break;
                    case (char)'*':
                        MultiplyButton.PerformClick();
                        e.Handled = true;
                        break;
                    case (char)'/':
                        DivideButton.PerformClick();
                        break;
                    case (char)'=':
                        EqualButton.PerformClick();
                        break;
                    case (char)13:
                        EqualButton.PerformClick();
                        break;
                    //WHITESPACE CHARACTER
                    case (char)8:
                        buttonhapus.PerformClick();
                        break;

                }

                e.Handled = true;
            
            

        }
        private void DisplayTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
          

                if (e.KeyChar >= 46 && e.KeyChar <= 59)
                {
                    DisplayTextBox.Text += e.KeyChar;
                    e.Handled = false;
                   
                }
                else if (e.KeyChar == '+' || e.KeyChar == '-' || e.KeyChar == '*' || e.KeyChar == '/' || e.KeyChar == '=')
                {
                    e.Handled = false;
                }

        }

        private void StoreButton_Click(object sender, EventArgs e)
        {
            save = Convert.ToDecimal(DisplayTextBox.Text);
            MessageBox.Show("Stored!");
        }

        private void RecallButton_Click(object sender, EventArgs e)
        {
            DisplayTextBox.Text = Convert.ToString(save);
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(DisplayTextBox.Text);
            MessageBox.Show("Copied to Clipboard");
        }
    
    }
}
